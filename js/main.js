(function ($) {
"use strict";
    
    // header sticky
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        if (scroll < 1) {
            $(".header-area").removeClass("sticky");
        } else {
            $(".header-area").addClass("sticky");
        }
    });
    
    // scrollIt (for header)
    $.scrollIt({
        upKey: 0, // key code to navigate to the next section
        downKey: 0, // key code to navigate to the previous section
        easing: 'linear', // the easing function for animation
        scrollTime: 900, // how long (in ms) the animation takes
        activeClass: 'active', // class given to the active nav element
        onPageChange: null, // function(pageIndex) that is called when page is changed
        topOffset: -30 // offste (in px) for fixed top navigation
    });
    
    // meanmenu for mobile menu
    $('#menu').meanmenu({
        meanMenuContainer: '.mean-menu-wrap',
        meanScreenWidth: "767",
        onePage: true,
    });
    
    // data-background (for background image)
    $("[data-background]").each(function () {
        $(this).css("background-image", "url(" + $(this).attr("data-background") + ")")
    });
    
    // testimonial-carousel
    $('.testimonial-carousel').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.testimonial-carousel-authors',
        dots: false,
        // dotsClass:'slick-modified-dots',
    });
    // testimonial-carousel-authors
    $('.testimonial-carousel-authors').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.testimonial-carousel',
        dots: true,
        centerMode: true,
        focusOnSelect: true,
        arrows: false,
        dots: false,
    });

    // WOW active
    new WOW().init();

})(jQuery);